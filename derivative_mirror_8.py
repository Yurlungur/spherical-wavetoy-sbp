#!/usr/bin/env python


import numpy as np
import pylab as pl
from numpy import linalg
from copy import copy
from scipy import interpolate #import KroghInterpolator BarycentricInterpolator



EVEN=True
ODD=False
order = 4
TheInterpolator=interpolate.KroghInterpolator
USE_SBP=True


q00 = -2.09329763466349871588733
q10 = 4.0398572053206615302160
q20 = -3.0597858079809922953240
q30 = 1.37319053865399486354933
q40 = -0.25996430133016538255400
q50 = 0.0
q60= 0.0

q01 = -0.31641585285940445272297
q11 = -0.53930788973980422327388
q21 = 0.98517732028644343383297
q31 = -0.05264665989297578146709
q41 = -0.113807251750624235013258
q51 = 0.039879767889849911803103
q61= -0.0028794339334846531588787

q02 = 0.13026916185021164524452
q12 = -0.87966858995059249256890
q22 =  0.38609640961100070000134
q32 = 0.31358369072435588745988
q42 = 0.085318941913678384633511
q52 = -0.039046615792734640274641
q62 = 0.0034470016440805155042908

q03 = -0.01724512193824647912172
q13 = 0.16272288227127504381134
q23 = -0.81349810248648813029217
q33 = 0.13833269266479833215645
q43 = 0.59743854328548053399616
q53 = -0.066026434346299887619324
q63 = -0.0017244594505194129307249

q04 = -0.00883569468552192965061
q14 = 0.03056074759203203857284
q24 = 0.05021168274530854232278
q34 = -0.66307364652444929534068
q44 = 0.014878787464005191116088
q54 = 0.65882706381707471953820
q64 = -0.082568940408449266558615

def first(f,x,parity):
    """
    Calculates the first derivative of a function f with respect to x,
    requires parity p, which must be even or odd.
    """
    dx = x[1]-x[0]
    f_copy = np.empty(len(f)+order)
    f_copy[order/2:-order/2] = f[:]
    if parity == EVEN:
        f_copy[:order/2] = np.array([i for i in reversed(f[:order/2])])
    else:
        f_copy[:order/2] = np.array([i for i in reversed(-f[:order/2])])
    # extrapolation B.C. are really really bad.
    my_interp = TheInterpolator(x[-2*order:],f[-2*order:])
    f_copy[-order/2:] = my_interp(np.array([x[-1]+(i+1)*dx for i in range(order/2)]))
    out = np.empty_like(f)
    if order == 2:
        out = (f_copy[2:] - f_copy[:-2])/(2*dx)
    elif order == 4:
        out = (-f_copy[4:] + 8*f_copy[3:-1] - 8*f_copy[1:-3] + f_copy[:-4])/(12*dx)
    elif order == 8:
        out = (1.0/dx)*(-(1./280)*f_copy[8:]
                        +(4./105)*f_copy[7:-1]
                        -(1./5)*f_copy[6:-2]
                        +(4./5)*f_copy[5:-3]
                        -(4./5)*f_copy[3:-5]
                        +(1./5)*f_copy[2:-6]
                        -(4./105)*f_copy[1:-7]
                        +(1./280)*f_copy[:-8])
                        
    else: 
        raise ValueError("Derivative order not defined..")
    if USE_SBP==True:
        if order == 2:
            out[-1] = (f[-1] - f[-2])/dx
        elif order == 4:
            out[-2]=(1.0/dx)*(-q01*f[-1]-q11*f[-2]
                                    -q21*f[-3]
                                    -q31*f[-4]
                                    -q41*f[-5]
                                    -q51*f[-6]-q61*f[-7])
            out[-2]=(1.0/dx)*(-q01*f[-1]
                                    -q11*f[-2]-q21*f[-3]
                                    -q31*f[-4]-q41*f[-5]
                                    -q51*f[-6]-q61*f[-7])
            out[-3]=(1.0/dx)*(-q02*f[-1]-q12*f[-2]
                                    -q22*f[-3]
                                     -q32*f[-4]-q42*f[-5]
                                    -q52*f[-6]-q62*f[-7])
            out[-4]=(1.0/dx)*(-q03*f[-1]-q13*f[-2]
                                    -q23*f[-3]
                                    -q33*f[-4]-q43*f[-5]
                                    -q53*f[-6]
                                    -q63*f[-7])
            out[-5]=(1.0/dx)*(-q04*f[-1]-q14*f[-2]
                                    -q24*f[-3]
                                    -q34*f[-4]-q44*f[-5]
                                    -q54*f[-6]
                                    -q64*f[-7])
        else: 
            raise ValueError("Derivative order not SBP..")
    return out



def second(f,x,parity):
    """
    Calculates the second derivative of a function f with respect to x,
    requires parity p, which must be even or odd.
    """
    if USE_SBP==True:
        return first(first(f,x,parity),x,not parity)
    dx = x[1]-x[0]
    f_copy = np.empty(len(f)+order)
    f_copy[order/2:-order/2] = f[:]
    if parity == EVEN:
        f_copy[:order/2] = np.array([i for i in reversed(f[:order/2])])
    else:
        f_copy[:order/2] = np.array([i for i in reversed(-f[:order/2])])
    # extrapolation B.C. are really really bad.
    my_interp = TheInterpolator(x[-order-1:],f[-order-1:])
    f_copy[-order/2:] = my_interp(np.array([x[-1]+(i+1)*dx for i in range(order/2)]))
    out = np.empty_like(f)
    if order == 2:
        out = (f_copy[2:] - 2*f_copy[1:-1] + f_copy[:-2])/(dx*dx)
    elif order == 4:
        out = (-f_copy[4:] + 16*f_copy[3:-1] - 30*f_copy[2:-2]+ 16*f_copy[1:-3] - f_copy[:-4])/(12*dx*dx)
    elif order == 8:
        out = (1.0/(dx*dx))*(-(1./560)*f_copy[8:]
                             +(8./315)*f_copy[7:-1]
                             -(1./5)*f_copy[6:-2]
                             +(8./5)*f_copy[5:-3]
                             -(205./72)*f_copy[4:-4]
                             +(8/5)*f_copy[3:-5]
                             -(1./5)*f_copy[2:-6]
                             +(8./315)*f_copy[1:-7]
                             -(1./560)*f_copy[:8])
    else: 
        raise ValueError("Derivative order not defined..")
    #if order == 4:
    #    out[-2] = (3*f[-1] + 10*f[-2] - 18*f[-3] + 6*f[-4] - f[-5])/(12*dx)
        #out[-1] = out[-2]
    x_extended = np.empty(len(x)+order)
    x_extended[order/2:-order/2] = x[:]
    x_extended[:order/2] = np.array([i for i in reversed([x[0]-(i+1)*dx for i in range(order/2)])])
    x_extended[-order/2:] = np.array([x[-1]+(i+1)*dx for i in range(order/2)])
    return out#,x_extended,f_copy


def set_to_zero_at_origin(f,x):
    """
    Sets the value of an even function at the origin,
    assuming that function to be a 4th-order polynomial.
    """
    out = copy(f)
    x_interp = np.concatenate((np.array([-i for i in reversed(x[:])]),
                               x[:]))
    f_interp = np.concatenate((np.array([i for i in reversed(f[:])]),
                               f[:]))
    my_interp = TheInterpolator(x_interp,f_interp)
    out[0] = my_interp(x[0])
    return out
