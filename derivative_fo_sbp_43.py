#!/usr/bin/env python


import numpy as np
import pylab as pl
from numpy import linalg
from copy import copy
from sbp_coeffs_43 import Q

EVEN=True
ODD=False
order = 4

def first(f,x,parity):
    """
    Calculates the first derivative of a function f with respect to x,
    requires parity p, which must be even or odd.
    """
    dx = x[1]-x[0]
    f_copy = np.empty(len(f)+order) # add extra ghost points on the
                                    # outer cells for simplicity
    f_copy[order/2:-order/2] = f[:]
    if parity == EVEN:
        f_copy[:order/2] = np.array([i for i in reversed(f[:order/2])])
    else: # if parity == ODD
        f_copy[:order/2] = np.array([i for i in reversed(-f[:order/2])])
    out = np.empty_like(f)
    # x_extended = np.empty(len(f)+order)
    # x_extended[order/2:-order/2] = x[:]
    # x_extended[:order/2] = np.array([-dx*(i+0.5) for i in reversed(range(order/2))])
    if order == 2:
        out = (f_copy[2:] - f_copy[:-2])/(2*dx)
    elif order == 4:
        out = (-f_copy[4:] + 8*f_copy[3:-1] - 8*f_copy[1:-3] + f_copy[:-4])/(12*dx)
    else: 
        raise ValueError("Derivative order not defined..")
    if order == 2:
        out[-1] = (f[-1] - f[-2])/dx
    elif order == 4:
        out[-Q.shape[1]:] = (-1.0/dx)*np.dot(Q,f[-Q.shape[0]:])
    else: 
        raise ValueError("Derivative order not SBP..")

    return out

