#!/usr/bin/env python

import numpy as np
import pylab as pl
import derivative_gundlach as der
print der.order
RESOLUTIONS = [100,200,400]
R_MAX=4
XS = map(lambda x: np.linspace(0,R_MAX,x),RESOLUTIONS)
DXS = map(lambda x: x[1]-x[0],XS)
if der.STAGGERED:
    XS = map(lambda x: x+0.5*(x[1]-x[0]),XS)
print XS

def get_y_even(x):
    return np.exp(-x*x)

def get_d1_even(x):
    return -2*np.exp(-x*x)*x

def get_y_odd(x):
    return x*get_y_even(x)

def get_d1_odd(x):
    return np.exp(-x*x) - 2*np.exp(-x*x)*x*x + 2*get_y_even(x)

Y_EVENS = map(lambda x: get_y_even(x), XS)
Y_ODDS = map(lambda x: get_y_odd(x), XS)

d1_evens = [None for i in RESOLUTIONS]
d1_odds = [None for i in RESOLUTIONS]

e1_evens = [None for i in RESOLUTIONS]
e1_odds = [None for i in RESOLUTIONS]

for i in range(len(XS)):
    d1_evens[i] = der.first(Y_EVENS[i],XS[i],der.EVEN)
    d1_odds[i] = der.first(Y_ODDS[i],XS[i],der.ODD)
    e1_evens[i] = (get_d1_even(XS[i]) - d1_evens[i])*RESOLUTIONS[i]**der.order
    e1_odds[i] = (get_d1_odd(XS[i]) - d1_odds[i])*RESOLUTIONS[i]**der.order

lines = [pl.plot(XS[i],d1_evens[i]) for i in range(len(XS))]
pl.title('d1 even')
pl.show()

lines = [pl.plot(XS[i],d1_odds[i]) for i in range(len(XS))]
pl.title('d1 odd')
pl.show()

lines = [pl.plot(XS[i],e1_evens[i]) for i in range(len(XS))]
pl.title('d1 error even')
pl.show()

lines = [pl.plot(XS[i],e1_odds[i]) for i in range(len(XS))]
pl.title('d1 error odd')
pl.show()
