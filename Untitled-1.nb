(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     17352,        586]
NotebookOptionsPosition[     15918,        530]
NotebookOutlinePosition[     16254,        545]
CellTagsIndexPosition[     16211,        542]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"(", 
    RowBox[{"1", "/", 
     SuperscriptBox["r", "2"]}], ")"}], " ", 
   RowBox[{"D", "[", 
    RowBox[{
     RowBox[{
      SuperscriptBox["r", "2"], " ", 
      RowBox[{"D", "[", 
       RowBox[{
        RowBox[{"\[Phi]", "[", "r", "]"}], ",", "r"}], "]"}]}], ",", "r"}], 
    "]"}]}], "//", " ", "Simplify"}]], "Input",
 CellChangeTimes->{{3.646504473446714*^9, 3.646504474533834*^9}, {
  3.6465045055841713`*^9, 3.646504548375423*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox[
   RowBox[{"2", " ", 
    RowBox[{
     FractionBox[
      RowBox[{"A", " ", 
       RowBox[{"Cos", "[", 
        RowBox[{"t", " ", "\[Omega]"}], "]"}], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"k", " ", "r", " ", 
          RowBox[{"Cos", "[", 
           RowBox[{"k", " ", "r"}], "]"}]}], "-", 
         RowBox[{"Sin", "[", 
          RowBox[{"k", " ", "r"}], "]"}]}], ")"}]}], 
      SuperscriptBox["r", "2"]], "[", "r", "]"}]}], "r"], "+", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{"-", 
     FractionBox[
      RowBox[{"A", " ", 
       RowBox[{"Cos", "[", 
        RowBox[{"t", " ", "\[Omega]"}], "]"}], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"2", " ", "k", " ", "r", " ", 
          RowBox[{"Cos", "[", 
           RowBox[{"k", " ", "r"}], "]"}]}], "+", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{"-", "2"}], "+", 
            RowBox[{
             SuperscriptBox["k", "2"], " ", 
             SuperscriptBox["r", "2"]}]}], ")"}], " ", 
          RowBox[{"Sin", "[", 
           RowBox[{"k", " ", "r"}], "]"}]}]}], ")"}]}], 
      SuperscriptBox["r", "3"]]}], ")"}], "[", "r", "]"}], "+", 
  RowBox[{"2", " ", 
   RowBox[{
    SuperscriptBox[
     RowBox[{"(", 
      FractionBox[
       RowBox[{"A", " ", 
        RowBox[{"Cos", "[", 
         RowBox[{"t", " ", "\[Omega]"}], "]"}], " ", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"k", " ", "r", " ", 
           RowBox[{"Cos", "[", 
            RowBox[{"k", " ", "r"}], "]"}]}], "-", 
          RowBox[{"Sin", "[", 
           RowBox[{"k", " ", "r"}], "]"}]}], ")"}]}], 
       SuperscriptBox["r", "2"]], ")"}], "\[Prime]",
     MultilineFunction->None], "[", "r", "]"}]}], "+", 
  FractionBox[
   RowBox[{"2", " ", 
    RowBox[{
     SuperscriptBox[
      RowBox[{"(", 
       FractionBox[
        RowBox[{"A", " ", 
         RowBox[{"Cos", "[", 
          RowBox[{"t", " ", "\[Omega]"}], "]"}], " ", 
         RowBox[{"Sin", "[", 
          RowBox[{"k", " ", "r"}], "]"}]}], "r"], ")"}], "\[Prime]",
      MultilineFunction->None], "[", "r", "]"}]}], "r"], "+", 
  RowBox[{
   SuperscriptBox[
    RowBox[{"(", 
     FractionBox[
      RowBox[{"A", " ", 
       RowBox[{"Cos", "[", 
        RowBox[{"t", " ", "\[Omega]"}], "]"}], " ", 
       RowBox[{"Sin", "[", 
        RowBox[{"k", " ", "r"}], "]"}]}], "r"], ")"}], "\[Prime]\[Prime]",
    MultilineFunction->None], "[", "r", "]"}]}]], "Output",
 CellChangeTimes->{{3.646504542025119*^9, 3.6465045486988783`*^9}, 
   3.646604531528798*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   FractionBox["1", "r"], " ", 
   RowBox[{"D", "[", 
    RowBox[{
     RowBox[{"r", " ", 
      RowBox[{"\[Phi]", "[", "r", "]"}]}], ",", " ", "r", ",", " ", "r"}], 
    "]"}]}], "//", "Simplify"}]], "Input",
 CellChangeTimes->{{3.646604256153407*^9, 3.646604288883347*^9}}],

Cell[BoxData[
 RowBox[{
  FractionBox[
   RowBox[{"2", " ", 
    RowBox[{
     FractionBox[
      RowBox[{"A", " ", 
       RowBox[{"Cos", "[", 
        RowBox[{"t", " ", "\[Omega]"}], "]"}], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"k", " ", "r", " ", 
          RowBox[{"Cos", "[", 
           RowBox[{"k", " ", "r"}], "]"}]}], "-", 
         RowBox[{"Sin", "[", 
          RowBox[{"k", " ", "r"}], "]"}]}], ")"}]}], 
      SuperscriptBox["r", "2"]], "[", "r", "]"}]}], "r"], "+", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{"-", 
     FractionBox[
      RowBox[{"A", " ", 
       RowBox[{"Cos", "[", 
        RowBox[{"t", " ", "\[Omega]"}], "]"}], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"2", " ", "k", " ", "r", " ", 
          RowBox[{"Cos", "[", 
           RowBox[{"k", " ", "r"}], "]"}]}], "+", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{"-", "2"}], "+", 
            RowBox[{
             SuperscriptBox["k", "2"], " ", 
             SuperscriptBox["r", "2"]}]}], ")"}], " ", 
          RowBox[{"Sin", "[", 
           RowBox[{"k", " ", "r"}], "]"}]}]}], ")"}]}], 
      SuperscriptBox["r", "3"]]}], ")"}], "[", "r", "]"}], "+", 
  RowBox[{"2", " ", 
   RowBox[{
    SuperscriptBox[
     RowBox[{"(", 
      FractionBox[
       RowBox[{"A", " ", 
        RowBox[{"Cos", "[", 
         RowBox[{"t", " ", "\[Omega]"}], "]"}], " ", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"k", " ", "r", " ", 
           RowBox[{"Cos", "[", 
            RowBox[{"k", " ", "r"}], "]"}]}], "-", 
          RowBox[{"Sin", "[", 
           RowBox[{"k", " ", "r"}], "]"}]}], ")"}]}], 
       SuperscriptBox["r", "2"]], ")"}], "\[Prime]",
     MultilineFunction->None], "[", "r", "]"}]}], "+", 
  FractionBox[
   RowBox[{"2", " ", 
    RowBox[{
     SuperscriptBox[
      RowBox[{"(", 
       FractionBox[
        RowBox[{"A", " ", 
         RowBox[{"Cos", "[", 
          RowBox[{"t", " ", "\[Omega]"}], "]"}], " ", 
         RowBox[{"Sin", "[", 
          RowBox[{"k", " ", "r"}], "]"}]}], "r"], ")"}], "\[Prime]",
      MultilineFunction->None], "[", "r", "]"}]}], "r"], "+", 
  RowBox[{
   SuperscriptBox[
    RowBox[{"(", 
     FractionBox[
      RowBox[{"A", " ", 
       RowBox[{"Cos", "[", 
        RowBox[{"t", " ", "\[Omega]"}], "]"}], " ", 
       RowBox[{"Sin", "[", 
        RowBox[{"k", " ", "r"}], "]"}]}], "r"], ")"}], "\[Prime]\[Prime]",
    MultilineFunction->None], "[", "r", "]"}]}]], "Output",
 CellChangeTimes->{{3.646604273946669*^9, 3.646604289149528*^9}, 
   3.646604531749754*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[Phi]", "=", 
  RowBox[{
   FractionBox["A", "r"], " ", 
   RowBox[{"Cos", "[", 
    RowBox[{"\[Omega]", " ", "t"}], "]"}], " ", 
   RowBox[{"Sin", "[", 
    RowBox[{"k", " ", "r"}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.6466024648312397`*^9, 3.6466024682984*^9}, {
  3.6466025576400557`*^9, 3.6466025672730627`*^9}, {3.646602828712031*^9, 
  3.6466029105278797`*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{"A", " ", 
   RowBox[{"Cos", "[", 
    RowBox[{"t", " ", "\[Omega]"}], "]"}], " ", 
   RowBox[{"Sin", "[", 
    RowBox[{"k", " ", "r"}], "]"}]}], "r"]], "Output",
 CellChangeTimes->{3.646602911335369*^9, 3.646604492871305*^9, 
  3.646604531755468*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"(", 
    RowBox[{"1", "/", 
     SuperscriptBox["r", "2"]}], ")"}], " ", 
   RowBox[{"D", "[", 
    RowBox[{
     RowBox[{
      SuperscriptBox["r", "2"], " ", 
      RowBox[{"D", "[", 
       RowBox[{"\[Phi]", ",", "r"}], "]"}]}], ",", "r"}], "]"}]}], "//", 
  "FullSimplify"}]], "Input",
 CellChangeTimes->{{3.646602887026676*^9, 3.646602923142468*^9}}],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{"A", " ", 
    SuperscriptBox["k", "2"], " ", 
    RowBox[{"Cos", "[", 
     RowBox[{"t", " ", "\[Omega]"}], "]"}], " ", 
    RowBox[{"Sin", "[", 
     RowBox[{"k", " ", "r"}], "]"}]}], "r"]}]], "Output",
 CellChangeTimes->{{3.646602902160186*^9, 3.646602923529334*^9}, 
   3.646604531814101*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"D", "[", 
  RowBox[{"\[Phi]", ",", "t", ",", "t"}], "]"}]], "Input",
 CellChangeTimes->{{3.6466029368914948`*^9, 3.646602944105171*^9}}],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{"A", " ", 
    SuperscriptBox["\[Omega]", "2"], " ", 
    RowBox[{"Cos", "[", 
     RowBox[{"t", " ", "\[Omega]"}], "]"}], " ", 
    RowBox[{"Sin", "[", 
     RowBox[{"k", " ", "r"}], "]"}]}], "r"]}]], "Output",
 CellChangeTimes->{3.646602944447455*^9, 3.646604531837234*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"D", "[", 
   RowBox[{"\[Phi]", ",", "r"}], "]"}], "//", "FullSimplify"}]], "Input",
 CellChangeTimes->{{3.646603378077921*^9, 3.6466034070100594`*^9}, {
  3.646603440624501*^9, 3.646603441240323*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{"A", " ", 
   RowBox[{"Cos", "[", 
    RowBox[{"t", " ", "\[Omega]"}], "]"}], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"k", " ", "r", " ", 
      RowBox[{"Cos", "[", 
       RowBox[{"k", " ", "r"}], "]"}]}], "-", 
     RowBox[{"Sin", "[", 
      RowBox[{"k", " ", "r"}], "]"}]}], ")"}]}], 
  SuperscriptBox["r", "2"]]], "Output",
 CellChangeTimes->{{3.646603386995926*^9, 3.646603407438078*^9}, 
   3.6466034419861517`*^9, 3.646604496453493*^9, 3.646604531920014*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"D", "[", 
    RowBox[{
     RowBox[{"D", "[", 
      RowBox[{"\[Phi]", ",", "r"}], "]"}], ",", "r"}], "]"}], " ", "+", " ", 
   RowBox[{
    FractionBox["2", "r"], 
    RowBox[{"D", "[", 
     RowBox[{"\[Phi]", ",", "r"}], "]"}]}]}], "//", "FullSimplify"}]], "Input",\

 CellChangeTimes->{{3.646603021499908*^9, 3.646603023859173*^9}, {
  3.646603070970601*^9, 3.646603104880251*^9}}],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{"A", " ", 
    SuperscriptBox["k", "2"], " ", 
    RowBox[{"Cos", "[", 
     RowBox[{"t", " ", "\[Omega]"}], "]"}], " ", 
    RowBox[{"Sin", "[", 
     RowBox[{"k", " ", "r"}], "]"}]}], "r"]}]], "Output",
 CellChangeTimes->{
  3.64660302417327*^9, {3.646603079091467*^9, 3.646603105383401*^9}, 
   3.6466045319476223`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"D", "[", 
  RowBox[{"\[Phi]", ",", "t"}], "]"}]], "Input",
 CellChangeTimes->{{3.646603054350664*^9, 3.646603057330654*^9}}],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox[
   RowBox[{"A", " ", "\[Omega]", " ", 
    RowBox[{"Sin", "[", 
     RowBox[{"k", " ", "r"}], "]"}], " ", 
    RowBox[{"Sin", "[", 
     RowBox[{"t", " ", "\[Omega]"}], "]"}]}], "r"]}]], "Output",
 CellChangeTimes->{3.646603057745792*^9, 3.646604531976838*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"D2", " ", "=", " ", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      RowBox[{
       RowBox[{"-", "1"}], "/", "2"}], ",", "0", ",", 
      RowBox[{"1", "/", "2"}], ",", "0", ",", "0", ",", "0", ",", "0"}], 
     "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"{", 
     RowBox[{"0", ",", 
      RowBox[{
       RowBox[{"-", "1"}], "/", "2"}], ",", "0", ",", 
      RowBox[{"1", "/", "2"}], ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{"{", 
     RowBox[{"0", ",", "0", ",", 
      RowBox[{
       RowBox[{"-", "1"}], "/", "2"}], ",", "0", ",", 
      RowBox[{"1", "/", "2"}], ",", "0", ",", "0"}], "}"}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{"{", 
     RowBox[{"0", ",", "0", ",", "0", ",", 
      RowBox[{
       RowBox[{"-", "1"}], "/", "2"}], ",", "0", ",", 
      RowBox[{"1", "/", "2"}], ",", "0"}], "}"}], ",", "\[IndentingNewLine]", 
    
    RowBox[{"{", 
     RowBox[{"0", ",", "0", ",", "0", ",", "0", ",", 
      RowBox[{
       RowBox[{"-", "1"}], "/", "2"}], ",", "0", ",", 
      RowBox[{"1", "/", "2"}]}], "}"}]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.6465964927089977`*^9, 3.6465966147661877`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"-", 
      FractionBox["1", "2"]}], ",", "0", ",", 
     FractionBox["1", "2"], ",", "0", ",", "0", ",", "0", ",", "0"}], "}"}], 
   ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", 
     RowBox[{"-", 
      FractionBox["1", "2"]}], ",", "0", ",", 
     FractionBox["1", "2"], ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", 
     RowBox[{"-", 
      FractionBox["1", "2"]}], ",", "0", ",", 
     FractionBox["1", "2"], ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", 
     RowBox[{"-", 
      FractionBox["1", "2"]}], ",", "0", ",", 
     FractionBox["1", "2"], ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0", ",", 
     RowBox[{"-", 
      FractionBox["1", "2"]}], ",", "0", ",", 
     FractionBox["1", "2"]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{{3.646596542744536*^9, 3.646596615539706*^9}, 
   3.646604531999164*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"D2", "//", "MatrixForm"}]], "Input",
 CellChangeTimes->{{3.6465965434149723`*^9, 3.646596545924267*^9}}],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {
      RowBox[{"-", 
       FractionBox["1", "2"]}], "0", 
      FractionBox["1", "2"], "0", "0", "0", "0"},
     {"0", 
      RowBox[{"-", 
       FractionBox["1", "2"]}], "0", 
      FractionBox["1", "2"], "0", "0", "0"},
     {"0", "0", 
      RowBox[{"-", 
       FractionBox["1", "2"]}], "0", 
      FractionBox["1", "2"], "0", "0"},
     {"0", "0", "0", 
      RowBox[{"-", 
       FractionBox["1", "2"]}], "0", 
      FractionBox["1", "2"], "0"},
     {"0", "0", "0", "0", 
      RowBox[{"-", 
       FractionBox["1", "2"]}], "0", 
      FractionBox["1", "2"]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{{3.646596546225769*^9, 3.646596588688883*^9}, 
   3.6465966186961517`*^9, 3.646604532022612*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"-", "D2"}], "//", "Transpose"}], "//", "MatrixForm"}]], "Input",
 CellChangeTimes->{{3.6465966213361473`*^9, 3.646596626812883*^9}}],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {
      FractionBox["1", "2"], "0", "0", "0", "0"},
     {"0", 
      FractionBox["1", "2"], "0", "0", "0"},
     {
      RowBox[{"-", 
       FractionBox["1", "2"]}], "0", 
      FractionBox["1", "2"], "0", "0"},
     {"0", 
      RowBox[{"-", 
       FractionBox["1", "2"]}], "0", 
      FractionBox["1", "2"], "0"},
     {"0", "0", 
      RowBox[{"-", 
       FractionBox["1", "2"]}], "0", 
      FractionBox["1", "2"]},
     {"0", "0", "0", 
      RowBox[{"-", 
       FractionBox["1", "2"]}], "0"},
     {"0", "0", "0", "0", 
      RowBox[{"-", 
       FractionBox["1", "2"]}]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.646596627142211*^9, 3.646604532043641*^9}]
}, Open  ]]
},
WindowSize->{1366, 751},
WindowMargins->{{0, Automatic}, {0, Automatic}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (December 4, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 490, 15, 37, "Input"],
Cell[1073, 39, 2588, 79, 211, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3698, 123, 310, 9, 56, "Input"],
Cell[4011, 134, 2586, 79, 211, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6634, 218, 398, 10, 55, "Input"],
Cell[7035, 230, 289, 8, 50, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7361, 243, 402, 13, 37, "Input"],
Cell[7766, 258, 351, 10, 52, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8154, 273, 160, 3, 32, "Input"],
Cell[8317, 278, 330, 9, 52, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8684, 292, 235, 5, 32, "Input"],
Cell[8922, 299, 513, 14, 51, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9472, 318, 431, 13, 56, "Input"],
Cell[9906, 333, 377, 11, 52, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10320, 349, 148, 3, 32, "Input"],
Cell[10471, 354, 304, 8, 50, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10812, 367, 1210, 33, 121, "Input"],
Cell[12025, 402, 1044, 30, 49, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[13106, 437, 128, 2, 32, "Input"],
Cell[13237, 441, 1242, 37, 166, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[14516, 483, 179, 4, 32, "Input"],
Cell[14698, 489, 1204, 38, 279, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

