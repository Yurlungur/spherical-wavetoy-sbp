#!/usr/bin/env python


"""
Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-07-24 15:24:41 (jmiller)>

This is the set of derivative operators defined in Gundlach et al. These
operators satisfy summation by parts in spherical symmetry.

We have two operators
(1/h) D approximates (d/dr)

(1/h) D_tilde approximates (d/dr) + (P/r)

where P is some constant. See:
doi:10.1088/0264-9381/30/14/145003
"""

import numpy as np
import pylab as pl
from numpy import linalg
from copy import copy
from sbp_coeffs_43 import Q

EVEN=True
ODD=False
STAGGERED=False
order = 4
P=2

OFFSET = 0 if STAGGERED else 1
DIAG_MIN=2 if STAGGERED else 4

if STAGGERED:
    FILENAME="u1_vbar_wbar__SBP4_staggered__p=02.dat"
else:
    FILENAME="u32_u52_vbar_wbar__SBP4_centred__p=02.dat"

def vbar_asymptotic(i,p):
    """
    Asymptotic solutions to recursion relations for vbar.
    """
    return (1 + (1.0/(60.*(i**4)))*((2*p-1)*(p-1)*p*(p+1)*(p+3))
            + (1.0/(504.*(i**6)))*((2*p-3)*(p-3)*(p-2)*(p-1)*p*(p+1)*(p+3)))

def wbar_asymptotic(i,p):
    """
    Asymptotic solutions to recursion relations for wbar.
    """
    return (1+ (1.0/(60.*(i**4)))*((2*p+1)*(p+1)*p*(p-1)*(p-3))
            + (1.0/(504.*(i**6)))*((2*p-1)*(p-5)*(p-3)*(p-2)*(p-1)*p*(p+1)))

def load_coeffs(imax = 2000):
    """
    Load u,wbar,vbar, as calculated by Gundlach et al.
    If need be, calculate asymptotic values. 
    """
    data = np.loadtxt(FILENAME)
    if STAGGERED:
        u_data = data[:1,...]
        data = data[1:,...]
    else:
        u_data = data[:2,...]
        data = data[2:,...]
    vbar = np.copy(data[...,0])
    wbar = np.copy(data[...,1])
    if imax > 2000:
        vbar.resize(imax)
        wbar.resize(imax)
        for i in range(2000,imax):
            vbar[i] = vbar_asymptotic(i,P)
            wbar[i] = wbar_asymptotic(i,P)
    ioffset = 0.5 if STAGGERED else 0
    indexes = np.array([(ioffset+i) for i in range(len(vbar))])
    v = vbar * indexes**P
    w = wbar * indexes**P
    return u_data,v,w

def extend_f(f,parity):
    """
    Adds ghost points to f based on parity
    """
    f_copy = np.empty(len(f)+order) # add extra ghost points on the
                                    # outer cells for simplicity
    f_copy[order/2:-order/2] = f[:]
    if parity == EVEN:
        f_copy[:order/2]\
            = np.array([i for i in reversed(f[OFFSET:order/2+OFFSET])])
    else: # if parity == ODD
        f_copy[:order/2]\
            = np.array([i for i in reversed(-f[OFFSET:order/2+OFFSET])])
    return f_copy

def D_f(f,parity):
    """
    Standard 42-order SBP derivative due to Strand. Missing 1/dr term.
    """
    f_copy = extend_f(f,parity)
    out = (1./12)*(-f_copy[4:] + 8*f_copy[3:-1] - 8*f_copy[1:-3] + f_copy[:-4])
    out[-4] = -(3./98)*f[-1] + (59./98)*f[-3] - (32./49)*f[-5] + (4./49)*f[-6]
    out[-3] = -(4./43)*f[-1] + (59./86)*f[-2] - (59./86)*f[-4] +(4./43)*f[-5]
    out[-2] = (1./2.)*f[-1] - (1./2.)*f[-3]
    out[-1] = (24./17.)*f[-1] - (59./34.)*f[-2] + (4./17)*f[-3] + (3./34)*f[-4]
    return out

def D_tilde_f(f,parity,v,w,wtilde_small):
    """
    Discretization of dr((d/dr) + (2/r). Missing 1/dr term.
    """
    M = len(f)
    W_tilde_F = v[:M]*f
    W_tilde_F[:DIAG_MIN] = np.dot(wtilde_small,f[:DIAG_MIN])
    D_W_tilde_F = D_f(W_tilde_F,parity)
    out = np.zeros_like(D_W_tilde_F)
    out[OFFSET:] = D_W_tilde_F[OFFSET:] / w[OFFSET:M]
    if not STAGGERED:
        out[0] =(1+P)*D_f(f,ODD)[0]
    return out

def energy(pi,psi,v,w,dr):
    """
    approximate energy of the system. pi is assumed to be even. psi odd.
    pi = (d/dt) phi, psi = (d/dr) phi
    """
    M = len(pi)
    W = copy(w[:M])
    W_tilde = copy(v[:M])
    W[-1] *= (17./48)
    W[-2] *= (59./48)
    W[-3] *= (43./48)
    W[-4] *= (49./48)
    W_tilde[-1] *= (17./48)
    W_tilde[-2] *= (59./48)
    W_tilde[-3] *= (43./48)
    W_tilde[-4] *= (49./48)
    return 0.5*(dr**(P+1))*(np.sum(pi*W*pi) + np.sum(psi*W_tilde*psi))

u_data,v,w = load_coeffs(3000)
if STAGGERED:
    u1 = u_data[0][0]
    wtilde_small = np.array([[v[0],u1],
                             [u1,v[1]]])
else:
    u32 = u_data[0,0]
    u52 = u_data[1,0]
    wtilde_small = np.array([[v[0], 0,    0,    0],
                             [0,    v[1], u32,  0],
                             [0,    u32,  v[2], u52],
                             [0,    0,    u52,  v[3]]])
    print wtilde_small

def first(f,x,parity):
    """
    Calculates the first derivative of a function f with respect to x
    in spherical symmetry. Requires parity, which must be even or
    odd.
    """
    dx = x[1]-x[0]
    if parity == EVEN:
        out = (1.0/dx) * D_f(f,parity)
    if parity == ODD:
        out = (1.0/dx) * D_tilde_f(f,parity,v,w,wtilde_small)
    return out

def impose_von_neumann_outer(psi,x,psiR):
    """
    Impose von-neumann boundary conditions on psi (which is odd)
    so that (1/dr)*D_tilde psi = psiR

    Modification is in-place.
    """
    M = len(psi)
    psi[-1] = (17.0/(24*v[M-1]))*(w[M-1]*psiR
                                 -(3./34.)*v[M-4]*psi[-4]
                                 -(4./17.)*v[M-3]*psi[-3]
                                 +(59./34.)*v[M-2]*psi[-2])
    return
